/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// SUM
function sum (a, b) { return a + b } 

let sumNum1 = 12
let sumNum2 = 3

let sumResult = sum(sumNum1, sumNum2)
console.log(`Displayed sum of ${sumNum1} and ${sumNum2}:`)
console.log(sumResult)

// DIFFERENCE
function difference (a, b) { return a - b }

let diffNum1 = 12
let diffNum2 = 45	

let diffResult = difference(diffNum1, diffNum2)
console.log(`Displayed difference of ${diffNum1} and ${diffNum2}:`)
console.log(diffResult)

// PRODUCT
function product (a, b) { return a * b }

let prodNum1 = 12
let prodNum2 = 4

let productResult = product(prodNum1, prodNum2)
console.log(`Displayed product of ${prodNum1} and ${prodNum2}:`)
console.log(productResult)

// QUOTIENT
function quotient (a, b) { return a / b}

let dividend = 10
let divisor = 2

let quotientResult = quotient(dividend, divisor) 
console.log(`Displayed quotient of ${dividend} and ${divisor}:`)
console.log(quotientResult)


// AREA OF CIRCLE
function getCircleArea(radius) { 
	return (Math.PI * radius * radius).toFixed(2) 
}

let radius = 15
let area = getCircleArea(radius)
console.log(`The result of getting the area of a circle with ${radius} radius:`)
console.log(area)
 
// AVERAGE
function getAverage(num1, num2, num3, num4) { 
	return (num1, num2, num3, num4) / 4 
} 

let num1 = 23
let num2 = 64
let num3 = 12
let num4 = 34

let average = getAverage(num1, num2, num3, num4)
console.log(`The Average of ${num1},${num2},${num3} and ${num4}:`)
console.log(average)

// DID PASS
function didPass (score, totalScore, passingPercentage = 70) {
	return score / totalScore >= passingPercentage / 100 
}

let score = 38
let totalScore = 50
let result = didPass(score, totalScore)
console.log(`Is ${score}/${totalScore} a passing score?`)
console.log(result)